# This file is a template, and might need editing before it works on your project.

#第一次引入镜像为打包go应用镜像
FROM golang:1.17.8 AS builder

WORKDIR /usr/src/app
#编译程序
COPY . .
RUN go build

##第二次引入镜像为 程序运行时的镜像
#FROM hub.dianchu.cc/library/gobase:stretch
#
#WORKDIR /app
##将编译好的程序从打包镜像中复制到运行镜像中
#COPY --from=builder /usr/src/app/GrandMainSer .
#ENTRYPOINT ["./GrandMainSer"]
#CMD ["-release"]
